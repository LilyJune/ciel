# frozen_string_literal: true

require 'discordrb'
require 'gitlab'
require 'pg'
require 'dotenv'
require 'ffmpeg'
require 'youtube-dl'
require 'time'
require_relative 'commands.rb'

SEC_DAY = 86_400

Dotenv.load

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = ENV['API_KEY']
end

# Module for the entirety of Ciel
module Ciel
  BOT = Discordrb::Commands::CommandBot.new token: ENV['TOKEN'], prefix: '/'

  Commands.include!

  at_exit do
    exit!
  end

  BOT.message do |event|
    user_id = event.author.id
    guild_id = event.channel.id

    begin
      conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
      conn.exec("INSERT INTO posts (user_id, guild_id, num_posts)
                 VALUES ('#{user_id}', '#{guild_id}', 1)
                 ON CONFLICT(user_id, guild_id)
                 DO UPDATE SET
                 num_posts = posts.num_posts + 1")
    rescue PG::Error => e
      puts "Message Tracking: #{e.message}"
    ensure
      conn&.close
    end
  end

  BOT.run
end
