# frozen_string_literal: true

require 'pg'
require 'dotenv'

Dotenv.load

conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
begin
  conn.exec('DROP TABLE posts')
  conn.exec('DROP TABLE currency')
rescue PG::Error => e
  puts "Message Tracking: #{e.message}"
ensure
  conn&.close
end
