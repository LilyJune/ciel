# frozen_string_literal: true

require 'pg'
require 'dotenv'

Dotenv.load

conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
begin
  conn.exec('CREATE TABLE IF NOT EXISTS posts(
             id SERIAL,
             user_id VARCHAR(255),
             guild_id VARCHAR(255),
             num_posts INT,
             date TIMESTAMP NOT NULL DEFAULT NOW(),
             PRIMARY KEY(user_id, guild_id)
             )')

  conn.exec('CREATE TABLE IF NOT EXISTS currency(
             user_id VARCHAR(255) NOT NULL,
             guild_id VARCHAR(255) NOT NULL,
             points INT,
             date TIMESTAMP NOT NULL DEFAULT NOW(),
             PRIMARY KEY(user_id, guild_id)
             )')
rescue PG::Error => e
  puts "Message Tracking: #{e.message}"
ensure
  conn&.close
end
