# frozen_string_literal: true

module Ciel
  # Module for commands
  module Commands
    Dir["#{File.dirname(__FILE__)}/commands/*.rb"].sort.each { |file| require file }

    @commands = [
      Avatar,
      Connect,
      Daily,
      DevInfo,
      Flip,
      Leave,
      Ping,
      Play,
      Profile,
      ReportIssue,
      Wallet
    ]

    def self.include!
      @commands.each do |command|
        Ciel::BOT.include!(command)
      end
    end
  end
end
