# frozen_string_literal: true

module Ciel
  module Commands
    # Module for leave command
    module Leave
      extend Discordrb::Commands::CommandContainer
      command(:leave) do |event|
        channel = event.user.voice_channel

        next 'Answer! You are not in a voice channel!' unless channel

        BOT.voice_destroy(event.server)
      end
    end
  end
end
