# frozen_string_literal: true

module Ciel
  module Commands
    # Module for play command
    module Play
      extend Discordrb::Commands::CommandContainer
      command(:play) do |event|
        channel = event.user.voice_channel
        next "You're not in any voice channel!" unless channel

        array = event.message.content.split(' ')
        if array.length == 2
          url = array[1]

          regex = %r{youtube.com.*(?:/|v=)([^&$]+)}
          name = url.match(regex)[1]
          name = "../tmp/#{name}.mp4"

          YoutubeDL.download url, format: 'mp4', output: name unless File.file?(name)

          BOT.voice_connect(event.author.voice_channel)
          event.channel.send_embed do |embed|
            embed.color = '#00FA9A'
            embed.title = 'Answer!'
            embed.description = "Now Playing #{url}"
            embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
            embed.timestamp = Time.now
          end

          'I\'ll be here for you later.' if event.voice.play_file(name)
        else
          event.channel.send_embed do |embed|
            embed.color = '#FF0000'
            embed.title = 'Answer!'
            embed.description = 'Please follow the format `play <url>`!'
            embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
            embed.timestamp = Time.now
          end
        end
      end
    end
  end
end
