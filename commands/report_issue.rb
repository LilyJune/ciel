# frozen_string_literal: true

module Ciel
  module Commands
    # Module for reportissue command
    module ReportIssue
      extend Discordrb::Commands::CommandContainer
      command(:reportissue) do |event|
        event.channel.send_embed do |embed|
          embed.color = '#C8E0EA'
          embed.title = 'Report an Issue'
          embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/17946509/avatar.png?width=40')
          embed.description = 'Please report any issue [here](https://gitlab.com/LilyJune/ciel/-/issues).'
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
          embed.timestamp = Time.now
        end
      end
    end
  end
end
