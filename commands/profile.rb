# frozen_string_literal: true

module Ciel
  module Commands
    # Module for profile command
    module Profile
      extend Discordrb::Commands::CommandContainer
      command(:profile) do |event|
        user_id = event.author.id
        guild_id = event.channel.id

        begin
          conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
          conn.exec("SELECT posts.num_posts, currency.points
               FROM posts
               INNER JOIN currency
               ON posts.user_id = currency.user_id
               AND posts.guild_id = currency.guild_id
               WHERE posts.user_id = '#{user_id}'
               AND posts.guild_id = '#{guild_id}'") do |result|
            if result.ntuples.positive?
              exp = result.getvalue(0, 0)
              wallet = result.getvalue(0, 1)
              event.channel.send_embed do |embed|
                embed.color = '#00FA9A'
                embed.title = event.author.username
                embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "#{event.author.avatar_url}?size=256")
                embed.description = "Total Experience: #{exp}\nWallet: #{wallet}"
                embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                embed.timestamp = Time.now
              end
            end
          end
        rescue PG::Error => e
          puts "Profile: #{e.message}"
        ensure
          conn&.close
        end
      end
    end
  end
end
