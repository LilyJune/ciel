# frozen_string_literal: true

module Ciel
  module Commands
    # Module for wallet command
    module Wallet
      extend Discordrb::Commands::CommandContainer

      def self.get_wallet(user_id, guild_id)
        begin
          conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
          conn.exec("SELECT points FROM currency WHERE user_id = '#{user_id}' AND guild_id = '#{guild_id}'") do |result|
            return result.getvalue(0, 0) if result.ntuples.positive?
          end
        rescue PG::Error => e
          puts "Message Tracking: #{e.message}"
          return 0
        ensure
          conn&.close
        end
        0
      end

      command(:wallet) do |event|
        user_id = event.author.id
        guild_id = event.channel.id

        wallet = get_wallet(user_id, guild_id)
        event.channel.send_embed do |embed|
          embed.color = '#00FA9A'
          embed.title = "#{event.author.username}'s Wallet"
          embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "#{event.author.avatar_url}?size=256")
          embed.description = "Wallet: #{wallet}"
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
          embed.timestamp = Time.now
        end
      end
    end
  end
end
