# frozen_string_literal: true

module Ciel
  module Commands
    # Module for Ping Command
    module Ping
      extend Discordrb::Commands::CommandContainer
      command(:ping) do |event|
        time = Time.now
        event.channel.send_embed do |embed|
          embed.color = '#00FA9A'
          embed.title = 'Ping'
          embed.description = "Request Serviced in #{((event.timestamp - time) * 1000).round(2)}ms!"
          embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "#{event.author.avatar_url}?size=256")
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: event.author.avatar_url)
          embed.timestamp = time
        end
      end
    end
  end
end
