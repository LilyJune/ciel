# frozen_string_literal: true

module Ciel
  module Commands
    # Module for avatar command
    module Avatar
      extend Discordrb::Commands::CommandContainer
      command(:avatar) do |event|
        event.channel.send_embed do |embed|
          embed.color = '#00FA9A'
          embed.title = "#{event.author.username}'s Avatar"
          embed.url = "#{event.author.avatar_url}?size=256"
          embed.image = Discordrb::Webhooks::EmbedImage.new(url: "#{event.author.avatar_url}?size=256")
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
          embed.timestamp = Time.now
        end
      end
    end
  end
end
