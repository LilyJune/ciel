# frozen_string_literal: true

module Ciel
  module Commands
    # Module for daily command
    module Daily
      extend Discordrb::Commands::CommandContainer

      def self.reduce_time(time_sec)
        time = time_sec.to_i
        hours = time / 3600
        time_min = time - (hours * 3600)
        min = time_min / 60
        time_sec = time_min - (min * 60)
        time_string = "#{time_sec} seconds"
        time_string = "#{min} minutes, " + time_string
        time_string = "#{hours} hours, " + time_string
        time_string
      end

      command(:daily) do |event|
        user_id = event.author.id
        guild_id = event.channel.id

        begin
          conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
          conn.exec("SELECT date
               FROM   currency
               WHERE  user_id = '#{user_id}'
               AND    guild_id = '#{guild_id}'
               ORDER  BY date ASC NULLS LAST
               LIMIT  1") do |result_time|
            if result_time.ntuples.positive?
              latest = result_time.getvalue(0, 0)
              now = Time.now
              time_diff = now - Time.parse(latest)
              if time_diff.to_i < SEC_DAY
                event.channel.send_embed do |embed|
                  embed.color = '#FF0000'
                  embed.title = 'Answer!'
                  embed.description = "Daily is not ready yet!\n#{reduce_time(SEC_DAY - time_diff)} left!"
                  embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                  embed.timestamp = now
                end
              else
                conn.exec("INSERT INTO currency (user_id, guild_id, points)
                     VALUES ('#{user_id}','#{guild_id}',100)
                     ON CONFLICT(user_id, guild_id)
                     DO UPDATE SET
                     points = currency.points + 100,
                     date = '#{Time.now}'")
                conn.exec("SELECT points
                     FROM currency
                     WHERE user_id = '#{user_id}'
                     AND guild_id = '#{guild_id}'") do |result|
                  pts = result.getvalue(0, 0)
                  event.channel.send_embed do |embed|
                    embed.color = '#00FA9A'
                    embed.title = 'Answer!'
                    embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "#{event.author.avatar_url}?size=256")
                    embed.description = "You have gained 100 points. \n Total Points: #{pts}"
                    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                    embed.timestamp = Time.now
                  end
                end
              end
            else
              conn.exec("INSERT INTO currency (user_id, guild_id, points)
                   VALUES ('#{user_id}','#{guild_id}',100)")
              conn.exec("SELECT points
                   FROM currency
                   WHERE user_id = '#{user_id}'
                   AND guild_id = '#{guild_id}'") do |result|
                pts = result.getvalue(0, 0)
                event.channel.send_embed do |embed|
                  embed.color = '#00FA9A'
                  embed.title = 'Answer!'
                  embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "#{event.author.avatar_url}?size=256")
                  embed.description = "You have gained 100 points. \n Total Points: #{pts}"
                  embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                  embed.timestamp = Time.now
                end
              end
            end
          end
        rescue PG::Error => e
          puts "Message Tracking: #{e.message}"
        ensure
          conn&.close
        end
      end
    end
  end
end
