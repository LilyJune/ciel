# frozen_string_literal: true

module Ciel
  module Commands
    # Module for devinfo command
    module DevInfo
      extend Discordrb::Commands::CommandContainer
      command(:devinfo) do |event|
        event.channel.send_embed do |embed|
          embed.color = '#C8E0EA'
          embed.title = 'Bot Dev'
          embed.description = "@#{Gitlab.user.username}"
          embed.url = "https://gitlab.com/#{Gitlab.user.username}"
          embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: "https://gitlab.com/uploads/-/system/user/avatar/#{Gitlab.user.id}/avatar.png")
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
          embed.timestamp = Time.now
        end
      end
    end
  end
end
