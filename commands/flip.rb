# frozen_string_literal: true

module Ciel
  module Commands
    # Module for flip command
    module Flip
      extend Discordrb::Commands::CommandContainer

      def self.get_wallet(user_id, guild_id)
        begin
          conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
          conn.exec("SELECT points FROM currency WHERE user_id = '#{user_id}' AND guild_id = '#{guild_id}'") do |result|
            return result.getvalue(0, 0) if result.ntuples.positive?
          end
        rescue PG::Error => e
          puts "Message Tracking: #{e.message}"
          return 0
        ensure
          conn&.close
        end
        0
      end

      def self.update_wallet(user_id, guild_id, amount)
        conn = PG.connect(dbname: ENV['PSQL_DB'], user: ENV['PSQL_USER'], password: ENV['PSQL_PASSWORD'])
        conn.exec("UPDATE currency
                 SET points = currency.points + #{amount},
                 date = '#{Time.now}'
                 WHERE user_id = '#{user_id}'
                 AND guild_id = '#{guild_id}'")
      rescue PG::Error => e
        puts "Message Tracking: #{e.message}"
      ensure
        conn&.close
      end

      def self.flip_bad_format(event)
        event.channel.send_embed do |embed|
          embed.color = '#FF0000'
          embed.title = 'Answer!'
          embed.description = 'Please follow the format `flip <h or t> <amount to bet>`!'
          embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
          embed.timestamp = Time.now
        end
      end

      command(:flip) do |event|
        user_id = event.author.id
        guild_id = event.channel.id

        array = event.message.content.split(' ')
        if array.length == 3 && (array[1].is_a? String) && array[2].number?
          choice = array[1]
          bet = array[2].to_i
          flip = rand(2)
          choice_bool = -1
          correct_format = false
          if choice == 'h'
            choice_bool = 1
            correct_format = true
          elsif choice == 't'
            choice_bool = 2
            correct_format = true
          else
            flip_bad_format(event)
          end
          if correct_format
            wallet = get_wallet(user_id, guild_id).to_i
            if bet <= wallet
              if choice_bool == flip
                update_wallet(user_id, guild_id, bet)
                event.channel.send_embed do |embed|
                  embed.color = '#00FA9A'
                  embed.title = 'Answer!'
                  embed.description = "You have won #{bet}!"
                  embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                  embed.timestamp = Time.now
                end
              else
                update_wallet(user_id, guild_id, 0 - bet)
                event.channel.send_embed do |embed|
                  embed.color = '#ff3300'
                  embed.title = 'Answer!'
                  embed.description = "You have lost #{bet}!"
                  embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                  embed.timestamp = Time.now
                end
              end
            else
              event.channel.send_embed do |embed|
                embed.color = '#FF0000'
                embed.title = 'Answer!'
                embed.description = "You only have #{wallet}!\nYou cannot bet #{bet}!"
                embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Requested by: #{event.author.username}", icon_url: "#{event.author.avatar_url}?size=256")
                embed.timestamp = Time.now
              end
            end
          end
        else
          flip_bad_format(event)
        end
      end
    end
  end
end

# Class for checking if a string is a number.
class Object
  def number?
    to_f.to_s == to_s || to_i.to_s == to_s
  end
end
