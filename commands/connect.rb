# frozen_string_literal: true

module Ciel
  module Commands
    # Module for connect command
    module Connect
      extend Discordrb::Commands::CommandContainer
      command(:connect) do |event|
        channel = event.user.voice_channel

        next 'Answer! You are not in a voice channel!' unless channel

        BOT.voice_connect(channel)
        "Connected to voice channel: #{channel.name}"
      end
    end
  end
end
